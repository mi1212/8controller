//
//  ProgressViewController.swift
//  8controlleProject
//
//  Created by Mikhail Chuparnov on 10.03.2023.
//

import UIKit
import SnapKit

final class ProgressViewController: UIViewController {
    
    let lessons = [
        Lesson(name: "Тренируем ноты", isDone: true),
        Lesson(name: "Учим аппликатуру", isDone: true),
        Lesson(name: "Учим темп(длительность нот)", isDone: true),
        Lesson(name: "Учим мелодии", isDone: false),
    ]
    
    private lazy var progressView = ProgressView(lessons: self.lessons)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupLayout()
    }
    
    private func setupLayout() {
        view.addSubview(progressView)
        
        progressView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }
}

