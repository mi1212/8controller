//
//  ProgressView.swift
//  8controlleProject
//
//  Created by Mikhail Chuparnov on 21.03.2023.
//

import UIKit
import SnapKit

final class ProgressView: UIView {
    
    var lessons = [Lesson]() {
        didSet{
            myLessonsView.lessons = lessons
        }
    }
    
    private let progressView = ProgressBarView()
    
    private let myLessonsView = MyLessonsView()
    
    convenience init(lessons: [Lesson]) {
        self.init(frame: .zero)
        self.lessons = lessons
        setupLayout()
        setupProperts()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupLayout() {
        addSubview(progressView)
        addSubview(myLessonsView)
        
        progressView.snp.makeConstraints { make in
            make.top.leading.trailing.equalToSuperview()
            make.height.equalToSuperview().multipliedBy(0.47)
        }
        
        myLessonsView.snp.makeConstraints { make in
            make.bottom.leading.trailing.equalToSuperview()
            make.top.equalTo(progressView.snp.bottom).inset(30)
        }
    }
    
    private func setupProperts() {
        myLessonsView.lessons = lessons
        
        let lessonsIsDone = lessons.filter { $0.isDone == true }.count
        
        let qtyOfLessons = lessons.count
        
        let progress: Double = Double(lessonsIsDone) / Double(qtyOfLessons)
        
        progressView.progressBar.setProgress(progress: progress, animated: false)
        progressView.progressBar.setupTitleData(lessonsIsDone: lessonsIsDone, qtyOfLessons: qtyOfLessons)
        
    }
    
}
